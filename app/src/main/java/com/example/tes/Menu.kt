package com.example.tes

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_menu.*

class Menu : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        alfatihah.setOnClickListener() {
            val intent = Intent(this@Menu, Alfatihah::class.java)
            startActivity(intent)
            annas.setOnClickListener() {
                val intent = Intent(this@Menu, Annas::class.java)
                startActivity(intent)}
        }
    }
}
